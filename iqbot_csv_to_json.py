import json
import csv

def iqBotCsv2Json(filePaths):

    Paths = filePaths.split(',')
    iqBotCsvPath = Paths[0]
    iqBotJsonPath = Paths[1]
    #Read csv and write field values into dictionary
    Dict= {}
    with open(iqBotCsvPath) as file_object:
        csv_reader = csv.DictReader(file_object)
        line_count = 0
        for row in csv_reader:
                Dict[line_count] = row
                line_count +=1

    #Write dictionary values into 
    json_output = {
        'APermit': {
            'Page 3': {
                'Section 1 Applicant Details': {

                    'One or more applicant': Dict[0]['One_or_more_applicant'],
                    'Name Individual or Contact Person': Dict[0]['Name_Individual_or_Contact_Person'],
                    'Suitable operator reference': Dict[0]['Suitable_operator_reference'],
                    'Organization Name': Dict[0]['Organization_Name'],
                    'ABN/ACN': Dict[0]['ABN_ACN'],
                    'Residential or registered business address': Dict[0]['Residential_or_registered_business_address'],
                    'Phone': Dict[0]['Phone'],
                    'Postal Address': Dict[0]['Postal_Address'],
                    'Email': Dict[0]['Email'],
                    'Email Correspondence': Dict[0]['Email_Correspondence'],
                },

                'Section 1.1 Nomination of Agent': {
                    'Do you want to nominate agent': Dict[0]['Do_you_want_to_nominate_agent'],
                    'Name of Agent': Dict[0]['Name_of_Agent'],
                    'Organization Name': Dict[0]['Organization_Name'],
                    'ABN/ACN': Dict[0]['ABN_ACN_Agent'],
                    'Postal_Address':  Dict[0]['Postal_Address_Agent'],
                    'Phone': Dict[0]['Phone_Agent'],
                    'Email': Dict[0]['Email_Agent'],
                }

            },
            'Page 4': {
            
                'Section 2 Details of ERA(s)': 

                [
                    {
                        'ERA Number': Dict[0]['ERA_Number_table'],
                        'Threshold': Dict[0]['Threshold_table'],
                        'Name of ERA': Dict[0]['Name_of_ERA'],
                        'Comply with eligibility criteria': Dict[0]['Comply_with_eligibility_criteria'],
                        'Comply with all standard conditions': Dict[0]['Comply_with_all_standard_conditions'],
                    }
                ],
                #Check this field in IQ Bot training group
                'Attached details of Standard Conditions': 'No',
            
                'Section 3 Description of Land' :
                {
                'Number' : Dict[0]['Number'],
                'Street name' : Dict[0]['Street_Name'],
                'Suburb/Town' : Dict[0]['Suburb_Town'],
                'PostCode' : Dict[0]['PostCode'],
                'Real Property Description Lot' : Dict[0]['Real_Property_Description_Lot'],
                'Real Property Description Plan' : Dict[0]['Real_Property_Description_Plan'],
                'GPS or other descriptor' : Dict[0]['GPS_or_other_descriptor'],
                'Port' : Dict[0]['Port'],
                'Project Name' : Dict[0]['Project_Name'],
                }
            

            },
            'Page 8' :{
            'Section 12 Take effect Date' :
            {
                'Decision Date' :Dict[0]['Decision_Date'],
                'Nominated Date' : Dict[0]['Nominated_Date'],
                'Nominated take effect Date' :	Dict[0]['Nominated_take_effect_Date']		 
            },
            'Section 13 Nomination of site contact' :
            {
                'Nominate the site contact' : Dict[0]['Nominate_the_site_contact'],
                'Title' : Dict[0]['Title'],
                'First Name' : Dict[0]['First_Name'],
                'Surname' : Dict[0]['Surname'],
                'Email Address' : Dict[0]['Email_Correspondence_site_contact'],
                'Email Correspondence' :Dict[0]['Email_Correspondence_site_contact'],
                'Phone': Dict[0]['Phone_site_contact']
            }	  
            
            },
            'Page 9' : {
            'Section 14 Nomination of Application contact' :
            {
                'Name or Position' : Dict[0]['Name_or_Position'],
                'Primary Phone' : Dict[0]['Primary_Phone'],
                'Secondary Phone' : Dict[0]['Secondary_Phone'],
                'Email address' : Dict[0]['Email_address_contact']
            }
            
            }
        }
    }

    #Convert dictionary with relevant json structure to output json file
    with open(iqBotJsonPath,'w') as outputfile:
        json.dump(json_output, outputfile)

iqBotCsvPath = 'C:/Users/Deon/Documents/Python Code/Json/iqbotexport1.csv'
iqBotJsonPath = 'C:/Users/Deon/Documents/Python Code/Json/iqbot.pdf.json'
paths = iqBotCsvPath+','+iqBotJsonPath

iqBotCsv2Json(paths)
